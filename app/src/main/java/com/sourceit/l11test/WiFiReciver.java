package com.sourceit.l11test;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.provider.Contacts;
import android.provider.Settings;
import android.support.annotation.ColorInt;
import android.widget.Toast;

/**
 * Created by alexeyburkun on 6/3/17.
 */

public class WiFiReciver extends BroadcastReceiver {

    public static final int ID = 101;

    @Override
    public void onReceive(Context context, Intent intent) {

     //   String state = isWiFiEnebled(context) ? "wifi enable" : "wifi disable";
        String state = isAirplaneModeOn(context) ? "wifi enable" : "wifi disable";

        Toast.makeText(context, state, Toast.LENGTH_SHORT).show();
    }

    private boolean isWiFiEnabled(Intent intent) {
        NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
        return networkInfo != null && networkInfo.isConnected();
    }

    private boolean isWiFiEnebled(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
    }

    private static boolean isAirplaneModeOn(Context context) {
        return Settings.Global.getInt(context.getContentResolver(),Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
    }
}
